import sys

if len(sys.argv) == 3:
    filas = int (sys.argv[1])
    columnas = int (sys.argv[2])
    
    if filas < 1 or filas > 9 or columnas < 1 or columnas > 9:
        print("Filas o columnas incorrectos!")
        print("Ejemplo: python tabla.py [1-9] [1-9]")
    else:
        for f in range(filas):
            for c in range(columnas):
                print(" * ", end='')
            print("\t")
    
else:
    print("Argumentos incorrectos!")
    print("Ejemplo: python tabla.py [1-9] [1-9]")